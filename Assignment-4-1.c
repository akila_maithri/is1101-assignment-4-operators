#include<stdio.h>

int main()
{
	int n1, n2, n3, sum;
	float average;
	
	printf("Enter 3 integers seperated by spaces: ");
	scanf("%d %d %d", &n1, &n2, &n3);
	
	sum = n1+n2+n3;
	average = (float)sum/3;
	
	printf("Sum is: %d\nAverage is: %.2f", sum, average);
	
	return 0;
}
