#include<stdio.h>

int main()
{
    int x,y;

    printf("Enter two numbers to swap seperated by spaces: ");
    scanf("%d %d", &x, &y);

    x = x + y;
    y = x - y;
    x = x - y;

    printf("\nNumbers swapped: %d %d", x, y);

    return 0;

}